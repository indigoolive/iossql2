#include "StdAfx.h"
#include "ProcessSQL.h"
#include "SMTPMail.h"
#include "wininet.h"
#include ".\processsql.h"

CProcessSQL::CProcessSQL(void)
{
	m_henv = NULL;
	m_hdbc = NULL;
	m_hstmt = NULL;
}

CProcessSQL::~CProcessSQL(void)
{
}

bool CProcessSQL::RunSQLEntry(CString& Entry, CLogIt& Log)
{
TCHAR inibuf[4096];
long n_inibuf = 4096;

CString SQLString;
CString SQLFilename;
CString Filename;
CString FTP;
CString SQLOption = "";
bool bAppendFile;
bool bIgnoreFetchError;

CFile MyOutput;
CString ColData;
CString RowData;

CString FormatString;
CString Error;
long Count = 0;
CSMTPMail Mail;
CString GetLastErrorMessage;
UINT FileFlags = CFile::modeCreate | CFile::modeWrite;

CString Delimiter = "|";

SQLRETURN sRet;

CFile SQLFile;
const ULONGLONG maxSQLFileLen = 4096;
ULONGLONG SQLFileLen;

char SQLFileReadBuf[maxSQLFileLen];
UINT SFRBBytesRead;
CString SQLFileRead;

const SQLSMALLINT max_columns = 100;
SQLSMALLINT num_columns;

const int sqlbufsize = 4096;
TCHAR sqlbuf[max_columns][sqlbufsize];
SQLLEN sqlbuflen[max_columns];
int i; //for loops

	m_Log = Log;

	m_Log.WriteLogFile((LPCTSTR)"Processing %s", false, Entry);

	GetPrivateProfileString(Entry, _T("SQL"), _T(""), &inibuf[0], n_inibuf, _T(".\\IOSSQL.ini"));
	SQLString = inibuf;
	SQLString.Trim();

	if (SQLString.IsEmpty()) //SQLFile code added 12AUG2015; WWP
		{
		//m_Log.WriteLogFile((LPCTSTR)"SQL string empty", false);

		GetPrivateProfileString(Entry, _T("SQLFilename"), _T(""), &inibuf[0], n_inibuf, _T(".\\IOSSQL.ini"));
		SQLFilename = inibuf;
		SQLFilename.Trim();

		if (SQLFilename.IsEmpty())
		{
			m_Log.WriteLogFile((LPCTSTR)"SQL string empty and no SQL Filename", true);
			return false;
		}

		if (!(SQLFile.Open(SQLFilename,CFile::modeRead)))
		{
			m_Log.WriteLogFile((LPCTSTR)"Cannot locate SQL filename", true);
			return false;
		}
		
		SQLFileLen = SQLFile.GetLength();

		if (SQLFileLen > maxSQLFileLen)
		{
			m_Log.WriteLogFile((LPCTSTR)"Programmer error - increase maxSQLFileLen", true);
			return false;
		}

		SQLFile.SeekToBegin();
		SFRBBytesRead = SQLFile.Read(SQLFileReadBuf, SQLFileLen);
		SQLFile.Close();

		SQLFileRead = CString(SQLFileReadBuf,SFRBBytesRead); 
		SQLFileRead.Trim();
		SQLString = SQLFileRead;

		}

	GetPrivateProfileString(Entry, _T("Option"), _T(""), &inibuf[0], 1024, _T(".\\IOSSQL.ini"));
	SQLOption = inibuf;
	SQLOption.Trim();

	GetPrivateProfileString(Entry, _T("File"), _T(""), &inibuf[0], 1024, _T(".\\IOSSQL.ini"));
	Filename = inibuf;
	Filename.Trim();

	if (Filename.IsEmpty())
		m_Log.WriteLogFile((LPCTSTR)"Filename string empty. No output", false);


	bAppendFile = 1 == GetPrivateProfileInt(Entry, _T("AppendFile"), 0, _T(".\\IOSSQL.ini"));

	if (bAppendFile)
		FileFlags |= CFile::modeNoTruncate;

	bIgnoreFetchError = 1 == GetPrivateProfileInt(Entry, _T("IgnoreFetchError"), 0, _T(".\\IOSSQL.ini"));

	GetPrivateProfileString(Entry, _T("FTP"), _T("0"), &inibuf[0], 1024, _T(".\\IOSSQL.ini"));
	FTP = inibuf;
	FTP.Trim();

	if (!DBConnect())
		{
		DBDisconnect();
		Mail.SendMail("Connection Error", "Unable to connect to database", m_Log);
		return false;
		}

	if (!SQLOption.IsEmpty())
		{
		FormatString = SQLString;
		SQLString.Format(FormatString, SQLOption);
		}

	if (Filename.IsEmpty())
		{
		sRet = SQLExecDirect(m_hstmt, (SQLTCHAR*) ((LPCTSTR) SQLString), SQL_NTS);
		if (!SQL_SUCCEEDED(sRet))
			{
			m_Log.WriteLogFile((LPCTSTR)"Error Running SQL: SQL=%s", true, SQLString);
			ProcessSQLError(GetLastErrorMessage);
			DBDisconnect();
			Error.Format("<BR> Error Running SQL for %s <BR> SQL = %s", Entry, SQLString);
			Error += "<BR><BR> Get Last Error Messages (if any):";
			Error += GetLastErrorMessage;
			Mail.SendMail("SQL Error", Error, m_Log);
			return false;
			}
		}
	else
		{
		sRet = SQLExecDirect(m_hstmt, (SQLTCHAR*) ((LPCTSTR) SQLString), SQL_NTS);

		if (!SQL_SUCCEEDED(sRet) && sRet != SQL_NO_DATA)
			{
			m_Log.WriteLogFile((LPCTSTR)"Error Running SQL: SQL=%s", true, SQLString);
			ProcessSQLError(GetLastErrorMessage);
			DBDisconnect();
			Error.Format("<BR> Error Running SQL for %s <BR> SQL = %s", Entry, SQLString);
			Error += "<BR><BR> Get Last Error Messages (if any):";
			Error += GetLastErrorMessage;
			Mail.SendMail("SQL Error", Error, m_Log);
			return false;
			}
		else if (sRet == SQL_NO_DATA)
			{
			m_Log.WriteLogFile((LPCTSTR)"No Records to Process: SQL=%s", true, SQLString);
			DBDisconnect();
			Error.Format("Unexpected SQL_NO_DATA while processing %s", Entry);
			Mail.SendMail("No Data", Error, m_Log);
			return true;
			}

			sRet = SQLNumResultCols(m_hstmt, &num_columns); //Grab number of columns

			if (!SQL_SUCCEEDED(sRet))
			{
				m_Log.WriteLogFile((LPCTSTR)"Error determining number of columns", true);
				ProcessSQLError(GetLastErrorMessage);
				DBDisconnect();
				return false;
			}

			if (num_columns > max_columns)
			{
				m_Log.WriteLogFile((LPCTSTR)"IOSSQL will not support more than %i columns", true, max_columns);
				ProcessSQLError(GetLastErrorMessage);
				DBDisconnect();
				return false;
			}

			if (!MyOutput.Open(Filename, FileFlags))
			{
				m_Log.WriteLogFile((LPCTSTR)"Error Creating output file", true);
				DBDisconnect();
				Error.Format("Unable to create output file %s for %s", Filename, Entry);
				Mail.SendMail("File creation error", Error, m_Log);
				return false;
			}

			if (bAppendFile)
				MyOutput.SeekToEnd();

			bool bHeaders = 1 == GetPrivateProfileInt(Entry, _T("Headers"), 0, _T(".\\IOSSQL.ini"));
			
			if (bHeaders)
			{
				CString Headr = "";
				SQLSMALLINT bufferLenUsed;
				TCHAR column_buf[4096];

				for (i = 0; i < num_columns; i++) //list all the columns
				{
					sRet = SQLColAttribute(m_hstmt, i+1, SQL_DESC_LABEL, &column_buf[0], 1024, &bufferLenUsed, NULL);
					if (!SQL_SUCCEEDED(sRet))
					{
						m_Log.WriteLogFile((LPCTSTR)"Error heading column %i", true, i+1);
						ProcessSQLError(GetLastErrorMessage);
						DBDisconnect();
						return false;
					}
					Headr = Headr + column_buf;

					if (i < num_columns - 1) //No delimiter last column
						Headr = Headr + Delimiter;

				}//for loop heading columns

				Headr += "\r\n"; //eoln
				MyOutput.Write(Headr, Headr.GetLength());
			}//if headers

			for (i = 0; i < num_columns; i++) //bind all the columns
			{
				sRet = SQLBindCol(m_hstmt, i+1, SQL_C_CHAR, &sqlbuf[i], sqlbufsize, &sqlbuflen[i]);
				if (!SQL_SUCCEEDED(sRet))
				{
					m_Log.WriteLogFile((LPCTSTR)"Error Binding Column %i", true, i+1);
					ProcessSQLError(GetLastErrorMessage);
					DBDisconnect();
					return false;
				}
			}//for loop binding columns

			sRet = SQLFetch(m_hstmt);
			if (!SQL_SUCCEEDED(sRet))
			{
				m_Log.WriteLogFile((LPCTSTR)"Fetch of data failed", true);
				DBDisconnect();
				if (!bIgnoreFetchError)
				{
					Error.Format("Unable to fetch data while processing %s", Entry);
					Mail.SendMail("Fetch failed", Error, m_Log);
				}
				return false;
			}

			while (SQL_SUCCEEDED(sRet))
			{
				RowData = "";
				for (i = 0; i < num_columns; i++) //fetch all the columns
				{
					if (sqlbuflen[i] == SQL_NULL_DATA)
						ColData = "";
					else
						ColData = sqlbuf[i];

					ColData.Remove('\r');
					ColData.Remove('\n');

					RowData = RowData + ColData;

					if (i < num_columns - 1) //No delimiter last column
						RowData = RowData + Delimiter;
				} //for each column
				
				RowData += "\r\n"; //eoln
				MyOutput.Write(RowData, RowData.GetLength());
				Count++;
				sRet = SQLFetch(m_hstmt);
			}//while pulling recs
		
			m_Log.WriteLogFile((LPCTSTR)"Extracted %d records", false, Count);
			MyOutput.Close();

		}//else filename empty

	DBDisconnect();

	if (FTP == "1")
		{
		if (false == FTPFile(Filename, GetLastErrorMessage))
			{
			CString Errors;
			Errors.Format("FTP Error for %s", Entry);
			Errors += GetLastErrorMessage;
			Mail.SendMail("FTP Error", Errors, m_Log);
			}
		}

	return true;
}

void CProcessSQL::ProcessSQLError(CString& GLEMessages)
{
SQLCHAR       SqlState[6], Msg[SQL_MAX_MESSAGE_LENGTH];
SQLINTEGER    NativeError;
SQLSMALLINT   i, MsgLen;
SQLRETURN     sRet;
CString State;
CString Message;
CString TempString;

	i = 1;
	while ((sRet = SQLGetDiagRec(SQL_HANDLE_STMT, m_hstmt, i, SqlState, &NativeError, Msg, sizeof(Msg), &MsgLen)) != SQL_NO_DATA)
		{
		State = SqlState;
		Message = Msg;
		m_Log.WriteLogFile((LPCTSTR)"SQL State = %s, SQL message = %s", true, State, Message);
		TempString.Format("<BR>%d: SQL State = %s, SQL message = %s", i, State, Message);
		GLEMessages += TempString;
      i++;
		}
}

void CProcessSQL::ProcessSQLConnectionError(void)
{
SQLCHAR       SqlState[6], Msg[SQL_MAX_MESSAGE_LENGTH];
SQLINTEGER    NativeError;
SQLSMALLINT   MsgLen;
SQLRETURN     sRet;
CString State;
CString Message;
CString TempString;

	sRet = SQLGetDiagRec(SQL_HANDLE_DBC, m_hdbc, 1, SqlState, &NativeError, Msg, sizeof(Msg), &MsgLen); 
	State = SqlState;
	if (MsgLen > 0)
		{
		Message = Msg;
		if (!Message.IsEmpty())
			{
			TempString.Format("Native Error#: %ld, SQL State = %s, SQL message = %s", NativeError, State, Message);
			m_Log.WriteLogFile((LPCTSTR) TempString, true);
			}
		else
			m_Log.WriteLogFile((LPCTSTR) "Empty error string for failed connection", true);
		}
	else
		m_Log.WriteLogFile((LPCTSTR) "Empty error string for failed connection", true);
}

bool CProcessSQL::DBConnect(void)
{
SQLRETURN sRet;
CString Host;
CString UserID;
CString Password;
TCHAR Temp[255];
CString DSN;
CString AllowEmpty;

	GetPrivateProfileString(_T("DataBase"), _T("DBHost"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
	Host = Temp;
	Host.Trim();

	GetPrivateProfileString(_T("DataBase"), _T("DBUser"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
	UserID = Temp;
	UserID.Trim();

	GetPrivateProfileString(_T("DataBase"), _T("DBPW"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
	Password = Temp;
	Password.Trim();

	GetPrivateProfileString(_T("DataBase"), _T("AllowEmpty"), _T("0"), &Temp[0], 255, _T(".\\IOSSQL.ini"));
	AllowEmpty = Temp;
	AllowEmpty.Trim();

	if (Host.IsEmpty())
			{
			m_Log.WriteLogFile((LPCTSTR)"Invalid DB Host name", 1);
			return false;
			}

	if (AllowEmpty == _T("0"))
		{
		if (UserID.IsEmpty() || Password.IsEmpty())
			{
			m_Log.WriteLogFile((LPCTSTR)"Invalid DB arguments", 1);
			return false;
			}

		DSN.Format(_T("DSN=%s;UID=%s;PWD=%s;"), Host, UserID, Password);
		}
	else
		{
		DSN.Format(_T("DSN=%s;"), Host);
		if (!UserID.IsEmpty())
			{
			DSN += _T("UID=");
			DSN += UserID;
			DSN += _T(";");
			}
		if (!Password.IsEmpty())
			{
			DSN += _T("PWD=");
			DSN += Password;
			DSN += _T(";");
			}
		}

	sRet = SQLAllocHandle(SQL_HANDLE_ENV, NULL, &m_henv);
	if (sRet != SQL_SUCCESS)
		{
		m_Log.WriteLogFile((LPCTSTR)"Error Allocing henv", true);
		return false;
		}

	SQLSetEnvAttr(m_henv, SQL_ATTR_ODBC_VERSION, (void*) SQL_OV_ODBC3, SQL_IS_INTEGER);

	sRet = SQLAllocHandle(SQL_HANDLE_DBC, m_henv, &m_hdbc);
	if (sRet != SQL_SUCCESS)
		{
		m_Log.WriteLogFile((LPCTSTR)"Error Allocing hdbc", true);
		return false;
		}

	sRet = SQLDriverConnect(m_hdbc, NULL, (SQLTCHAR*) ((LPCTSTR) DSN), SQL_NTS, NULL, 0, NULL, SQL_DRIVER_COMPLETE);
	switch (sRet)
		{
		case SQL_SUCCESS:
		case SQL_SUCCESS_WITH_INFO:
			{
//			m_Log.WriteLogFile((LPCTSTR)"Connected to database %s", 0, Host);
			SQLRETURN sRet = SQLSetConnectAttr(m_hdbc, SQL_ATTR_AUTOCOMMIT, (SQLPOINTER) SQL_AUTOCOMMIT_ON, SQL_IS_INTEGER);
			if (!SQL_SUCCEEDED(sRet))
				{
				m_Log.WriteLogFile((LPCTSTR)"Error Setting Autocommit", true);
				return false;
				}
			break;
			}

		case SQL_NO_DATA:
			{
			// User canceled or other event for which we have no
			//  information.
			return false;
			break;
			}

		default:
			{
			ProcessSQLConnectionError();
			return false;
			}
		}

	sRet = SQLAllocHandle(SQL_HANDLE_STMT, m_hdbc, &m_hstmt);
	if (sRet != SQL_SUCCESS)
		{
		m_Log.WriteLogFile((LPCTSTR)"Error Allocing hstmt", true);
		return false;
		}

	return true;
}

bool CProcessSQL::DBDisconnect(void)
{
	if (m_hstmt != NULL)
		{
		SQLFreeHandle(SQL_HANDLE_STMT, m_hdbc);
		}

	if (m_hdbc != NULL)
		{
		SQLDisconnect(m_hdbc);
		SQLFreeHandle(SQL_HANDLE_DBC, m_hdbc);
		}

	if (m_henv != NULL)
		SQLFreeHandle(SQL_HANDLE_ENV, m_henv);

	m_hdbc = NULL;
	m_henv = NULL;
	m_hstmt = NULL;

	return true;
}


bool CProcessSQL::FTPFile(CString& Filename, CString& ErrorMessages)
{
HINTERNET hInternet = NULL;
HINTERNET hConnect = NULL;
CString Host;
CString UserID;
CString Password;
int DeleteLocal;
char Temp[255];
bool bReturn = true;
CString Errors;

	GetPrivateProfileString("FTP", "FTPHost", "", &Temp[0], 255, ".\\IOSSQL.ini");
	Host = Temp;
	Host.Trim();

	GetPrivateProfileString("FTP", "FTPUser", "", &Temp[0], 255, ".\\IOSSQL.ini");
	UserID = Temp;
	UserID.Trim();

	GetPrivateProfileString("FTP", "FTPPW", "", &Temp[0], 255, ".\\IOSSQL.ini");
	Password = Temp;
	Password.Trim();

	DeleteLocal = GetPrivateProfileInt("FTP", "FTPDeleteLocal", 0, ".\\IOSSQL.ini");

	if (Host.IsEmpty() || UserID.IsEmpty() || Password.IsEmpty())
		{
		m_Log.WriteLogFile((LPCTSTR)"Invalid FTP arguments", 1);
		return false;
		}


	hInternet = InternetOpen( "IOSFTP", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 1 );
	if (NULL == hInternet)
		{
		ProcessFTPError(Errors);

		m_Log.WriteLogFile((LPCTSTR)"Error opening internet connection", 1);

		ErrorMessages = "<BR>Error opening internet connection";
		ErrorMessages += "<BR><BR>Get Last Error Messages (if any): ";
		ErrorMessages += Errors;
		bReturn = false;
		}

	if (bReturn)
		{
		hConnect = InternetConnect( hInternet, Host, INTERNET_DEFAULT_FTP_PORT, UserID, Password, INTERNET_SERVICE_FTP, 0, 2);
		if (NULL == hConnect)
			{
			m_Log.WriteLogFile((LPCTSTR)"Error connecting to FTP site will try again in 5 seconds", 1);
			// try again
			Sleep(5000);
			hConnect = InternetConnect( hInternet, Host, INTERNET_DEFAULT_FTP_PORT, UserID, Password, INTERNET_SERVICE_FTP, 0, 2);
			if (NULL == hConnect)
				{
				ProcessFTPError(Errors);

				m_Log.WriteLogFile((LPCTSTR)"Error connecting to FTP site %s", 1, Host);
				
				ErrorMessages.Format("<BR>Error connecting to FTP site %s", Host);
				ErrorMessages += "<BR><BR>Get Last Error Messages (if any): ";
				ErrorMessages += Errors;
				bReturn = false;
				}
			}
		}

	if (bReturn)
		{
		// Place the actual data file
		if ( !FtpPutFile(hConnect, Filename, Filename, FTP_TRANSFER_TYPE_BINARY, 3))
			{
			m_Log.WriteLogFile((LPCTSTR)"Failed to PUT file %s will try again in 5 seconds", 1, Filename );
			// try again
			Sleep(5000);
			if ( !FtpPutFile(hConnect, Filename, Filename, FTP_TRANSFER_TYPE_BINARY, 3))
				{
				ProcessFTPError(Errors);
	
				m_Log.WriteLogFile((LPCTSTR)"Failed to PUT file %s", 1, Filename);

				ErrorMessages.Format("<BR>Failed to PUT file %s", Filename);
				ErrorMessages += "<BR><BR>Get Last Error Messages (if any): ";
				ErrorMessages += Errors;
				bReturn = false;
				}
			}
		}

	if (bReturn)
		{
		m_Log.WriteLogFile((LPCTSTR)"File sent - %s", 0, Filename );
		if (DeleteLocal == 1)
			DeleteFile(Filename);
		}

	if (hConnect != NULL)
		InternetCloseHandle(hConnect);

	if (hInternet != NULL)
		InternetCloseHandle(hInternet);

	return bReturn;
}



void CProcessSQL::ProcessFTPError(CString& ErrorString)
{
DWORD dwError = GetLastError();
LPTSTR lpBuffer;

	if (dwError == ERROR_INTERNET_EXTENDED_ERROR)
		{
		TCHAR ErrorBuffer[2048];
		DWORD BufferSize = 2047;
		if (InternetGetLastResponseInfo(&dwError, &ErrorBuffer[0], &BufferSize))
			{
			m_Log.WriteLogFile((LPCTSTR)"%s", 1, ErrorBuffer);
			ErrorString.Format("<BR>%s", ErrorBuffer);
			}
		}
	else
		{
		if (0 != ::FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
										NULL, dwError, MAKELANGID(LANG_NEUTRAL, SUBLANG_SYS_DEFAULT),
										(LPTSTR) &lpBuffer, 0, NULL))
			{
			m_Log.WriteLogFile((LPCTSTR)"%s", 1, lpBuffer);
			ErrorString.Format("<BR>%s", lpBuffer);
			}
		else
			{
			m_Log.WriteLogFile((LPCTSTR)"Error ID = %d", 1, dwError);
			ErrorString.Format("<BR>Error ID = %d", dwError);
			}
		}

	return;
}
