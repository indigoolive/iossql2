#pragma once

#include "sql.h"
#include "sqlext.h"
#include "LogIt.h"


class CProcessSQL
	{
	public:
		CProcessSQL(void);
		~CProcessSQL(void);
		bool RunSQLEntry(CString& Entry, CLogIt& Log);

	private:
		CString m_DSN;
		SQLHENV m_henv;
		SQLHDBC  m_hdbc;
		SQLHSTMT m_hstmt;
		CLogIt m_Log;

		bool DBConnect(void);
		bool DBDisconnect(void);
		bool FTPFile(CString& Filename, CString& ErrorMessages);
		void ProcessSQLError(CString& GLEMessages);
		void ProcessSQLConnectionError(void);
		void ProcessFTPError(CString& ErrorString);
	};
