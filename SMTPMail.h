#pragma once
#include "LogIt.h"

class CSMTPMail
{
public:
	CSMTPMail(void);
	~CSMTPMail(void);

	BOOL SendMail(LPCTSTR szSubject, LPCTSTR szMessage, CLogIt& Log);

private:
	void ReportSocketError(int TheError, CLogIt& Log);
};
