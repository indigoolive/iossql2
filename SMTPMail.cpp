#include "StdAfx.h"
#include ".\smtpmail.h"
#include "SocketX.h"


CSMTPMail::CSMTPMail(void)
{

}


CSMTPMail::~CSMTPMail(void)
{
}

BOOL CSMTPMail::SendMail(LPCTSTR szSubject, LPCTSTR szMessage, CLogIt& Log)
{
CString MailServer;
CString ToAddress;
CString FromAddress;
CString Company;
CString Subject;
CString Prefix;
CString Auth1, Auth2; //These don't do anything; WWP; 21AUG2015 ???
CString Message;
CSocketX MailSocket;
CString Command;
CString Response;
UINT Timeout = 5000;
char Temp[255];
CString EndString = "\r\n";
int nPort = 25;

	// Get Config Entries
	GetPrivateProfileString(_T("Mail"), _T("MailServer"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
		MailServer = Temp;
		MailServer.Trim();

	GetPrivateProfileString(_T("Mail"), _T("To"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
		ToAddress = Temp;	
		ToAddress.Trim();

	GetPrivateProfileString(_T("Mail"), _T("From"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
		FromAddress = Temp;
		FromAddress.Trim();

	GetPrivateProfileString(_T("Mail"), _T("SubjectPrefix"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
		Prefix = Temp;
		Prefix.Trim();

	GetPrivateProfileString(_T("Mail"), _T("Auth1"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
		Auth1 = Temp;
		Auth1.Trim();

	GetPrivateProfileString(_T("Mail"), _T("Auth2"), _T(""), &Temp[0], 255, _T(".\\IOSSQL.ini"));
		Auth2 = Temp;
		Auth2.Trim();

    nPort = (int)GetPrivateProfileInt(_T("Mail"), _T("Port"), 25, _T(".\\IOSSQL.ini"));

	if (!MailSocket.Create())
		{
		Log.WriteLogFile("Unable to create email socket", true);
		return FALSE;
		}

	// Connect to the server
	if (!MailSocket.Connect(MailServer, nPort))
		{
		Log.WriteLogFile("Could not connect to server - %s", true, MailServer);
		return FALSE;
		}

	// Read the "HELO" response from the server
	if (SOCKET_ERROR == MailSocket.Receive(Response, EndString, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}

   // Sanitiy check
	if (Response.Left(3) != _T("220"))
		{
		Log.WriteLogFile("Invalid server - %s", true, Response);
		MailSocket.Send("QUIT\r\n");
		return FALSE;
		}

	// Send our HELO
	Command = "HELO\r\n";
	MailSocket.Send(Command, Timeout);

	// and check the response
	if (SOCKET_ERROR == MailSocket.Receive(Response, EndString, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}

	// Send the "FROM" line
	Command.Format("MAIL FROM:%s\r\n", FromAddress);
	MailSocket.Send(Command, Timeout);
	
	// and check the response
	if (SOCKET_ERROR == MailSocket.Receive(Response, EndString, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}

	// Send the "RCPT" line
	Command.Format("RCPT TO:%s\r\n", ToAddress);
	MailSocket.Send(Command);

	// and check the response
	if (SOCKET_ERROR == MailSocket.Receive(Response, EndString, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}

/*
	// Extra Checking
	if (Response.Left(3) != _T("250"))
		{
		Log.WriteLogFile("Recipient rejected - %s", true, Response);
		MailSocket.Send("QUIT\r\n");
		return FALSE;
	}
*/

	// Send the "DATA" line
	MailSocket.Send("DATA\r\n");

	// and check the response
	if (SOCKET_ERROR == MailSocket.Receive(Response, EndString, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}
/*
	// Extra Checking
	if (Response.Left(3) != _T("354"))
		{
		Log.WriteLogFile("DATA command rejected - %s", true, Response);
		MailSocket.Send("QUIT\r\n");
		return FALSE;
		}
*/

	Subject = Prefix;
	Subject += " - ";
	Subject += szSubject;
	Message.Format("Error Message: %s", szMessage);
	// Send body
	Command.Format("Content-type: text/html\r\nTO:%s\r\nFrom:%s\r\nSubject: %s\r\n%s\r\n",
				ToAddress, FromAddress, Subject, Message);
	if (SOCKET_ERROR == MailSocket.Send(Command, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}

	// Send Termination
	MailSocket.Send("\r\n.\r\n");

	// and check the response
	if (SOCKET_ERROR == MailSocket.Receive(Response, EndString, Timeout))
		{
		ReportSocketError(MailSocket.GetLastError(), Log);
		return FALSE;
		}

	// Send the "QUIT" line
	MailSocket.Send("QUIT\r\n");
	return TRUE;
}

void CSMTPMail::ReportSocketError(int TheError, CLogIt& Log)
{
CString ErrorMessage;

	ErrorMessage.LoadString(TheError);
	Log.WriteLogFile("Socket Error - %s", true, ErrorMessage);
	return;
}
