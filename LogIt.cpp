//define NO_FLUSH to keep import lines together 
//and not have the file sorted by time
#include "stdafx.h"
#include "LogIt.h"
#include ".\logit.h"


CLogIt::CLogIt()
{
	m_ErrorText = "ERROR: ";
	m_OkText = "OK   : ";
	m_pFile = NULL;
}

CLogIt::~CLogIt()
{
}

bool CLogIt::Init(LPCTSTR LogPrefix, bool bDateLogging)
{
CString LogFile = LogPrefix;
CString Filename;
CString FileToClean;

	m_ct = CTime::GetCurrentTime();

	LogFile.Trim();
	if (LogFile.IsEmpty())
		LogFile = "LogIt";

	if (bDateLogging)
		{
		Filename.Format( ".\\%s.%.2d%.2d.log", LogFile, m_ct.GetMonth(), m_ct.GetDay() );
		m_TimePrefix = "%H:%M:%S - ";
		}
	else
		{
		Filename.Format( ".\\%s.log", LogFile);
		m_TimePrefix = "%m-%d-%y %H:%M:%S - ";
		}

	m_pFile = new CFile();

	if (!m_pFile->Open(Filename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::osWriteThrough, NULL))
		return false;
	else
		{
		m_pFile->SeekToEnd();
		WriteLogFile("Logging Initialized", false); 
		}

	if (bDateLogging)
		{
		// we are only going to keep 60 days worth of data
		m_ct -= CTimeSpan(60,0,0,0);
		FileToClean.Format( ".\\%s.%.2d%.2d.log", LogFile, m_ct.GetMonth(), m_ct.GetDay() );
		DeleteFile(FileToClean);
		}
	return true;
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError )
{
CString csOutput;

	m_ct = CTime::GetCurrentTime();
	
	csOutput = bError ? m_ErrorText : m_OkText;
	csOutput += m_ct.Format((LPCSTR)m_TimePrefix);

	csOutput += szEntry;
	csOutput += "\r\n";

	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, LPCTSTR attr1 )
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1 );
	csOutput += "\r\n";

	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, LPCTSTR attr1, LPCTSTR attr2 )
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1, attr2 );
	csOutput += "\r\n";

	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, LPCTSTR attr1, LPCTSTR attr2, LPCTSTR attr3 )
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1, attr2, attr3 );
	csOutput += "\r\n";

	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, int attr1)
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1 );
	csOutput += "\r\n";
	
	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, int attr1, int attr2)
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1, attr2 );
	csOutput += "\r\n";
	
	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, int attr1, int attr2, int attr3)
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1, attr2, attr3 );
	csOutput += "\r\n";
	
	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, DWORD attr1)
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1 );
	csOutput += "\r\n";
	
	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::WriteLogFile( LPCTSTR szEntry, bool bError, double attr1)
{
CString csOutput;
CString csFormat;

	m_ct = CTime::GetCurrentTime();

	csFormat = bError ? m_ErrorText : m_OkText;
	csFormat += m_ct.Format((LPCSTR)m_TimePrefix);

	csFormat += szEntry;
	csOutput.Format( csFormat, attr1 );
	csOutput += "\r\n";
	
	m_pFile->Write(csOutput, csOutput.GetLength());
	printf( csOutput );
}

void CLogIt::End(void)
{
	if (m_pFile != NULL)
		m_pFile->Close();

	delete(m_pFile);
}
