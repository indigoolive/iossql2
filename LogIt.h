#ifndef _CLOGIT_H
#define _CLOGIT_H

class CLogIt {
public:
	CLogIt();
	~CLogIt();
	void WriteLogFile( LPCTSTR szEntry, bool bError = false );
	void WriteLogFile( LPCTSTR szEntry, bool bError, LPCTSTR attr1 );
	void WriteLogFile( LPCTSTR szEntry, bool bError, LPCTSTR attr1, LPCTSTR attr2 );
	void WriteLogFile( LPCTSTR szEntry, bool bError, LPCTSTR attr1, LPCTSTR attr2, LPCTSTR attr3 );
	void WriteLogFile( LPCTSTR szEntry, bool bError, int attr1);
	void WriteLogFile( LPCTSTR szEntry, bool bError, int attr1, int attr2);
	void WriteLogFile( LPCTSTR szEntry, bool bError, int attr1, int attr2, int attr3);
	void WriteLogFile( LPCTSTR szEntry, bool bError, DWORD attr1);
	void WriteLogFile( LPCTSTR szEntry, bool bError, double attr1);

private:
	CTime m_ct;
//	FILE *m_fp;
//	CString m_filename;
	CString m_TimePrefix;
	bool m_bOneLogFile;
	CString m_ErrorText;
	CString m_OkText;
	CFile* m_pFile;


public:
	bool Init(LPCTSTR LogPrefix, bool bDateLogging = true);
	void End(void);
	};

#endif