// SOCKETX.H -- Extension of the CSocket class
//

#ifndef __SOCKETX_H__
#define __SOCKETX_H__

#include "afxsock.h"

class CSocketX : public CSocket
{
	DECLARE_DYNAMIC(CSocketX);

// Implementation
public:
	int Send(LPCTSTR lpszStr, UINT uTimeOut = 10000, int nFlags = 0);
	int Receive(CString& str, CString& EndString, UINT uTimeOut = 10000, int nFlags = 0);
	BOOL Connect(LPCTSTR szServer, UINT uPort, UINT uTimeOut = 10000);

protected: 
	virtual BOOL OnMessagePending();

private: 
	int m_Step;
	UINT m_TimeOut;
	DWORD m_StartTickCount;
};
#endif // __SOCKETX_H__
