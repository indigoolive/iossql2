// IOSSQL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "IOSSQL.h"
#include "LogIt.h"
#include "ProcessSQL.h"
#include "afxsock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
CString Section;
CProcessSQL Proc;
int nReturnCode;
CLogIt Log;
bool bDateLogging = true;

	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
		{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		nReturnCode = 1;
		}

	AfxSocketInit();

	if (argc < 2)
		{
		cout << "Usage: IOSSQL SQLSection [nodatelogs]" << endl;
		nReturnCode = 1;
		}
	
	Section = argv[1];
	Section.Trim();
	if (Section.IsEmpty())
		{
		cout << "Usage: IOSSQL SQLSection [nodatelogs]" << endl;
		nReturnCode = 1;
		}
	else
		{
		if (argc == 3)
			{
			CString DateLogs = argv[2];
			DateLogs.Trim();
			if (0 == DateLogs.CompareNoCase("nodatelogs"))
				bDateLogging=false; //WWP - 12AUG2015
			}

		Log.Init("IOSSQL", bDateLogging);

		if (Proc.RunSQLEntry(Section, Log))
			nReturnCode = 0;
		else
			nReturnCode = 1;
		Log.End();
		}

	return nRetCode;
}
