// SOCKETX.CPP -- Extension of the CSocket class
//

#include "stdafx.h"
#include "socketx.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CSocketX, CSocket)

BOOL CSocketX::Connect(LPCTSTR szServer, UINT uPort, UINT uTimeOut)
{
BOOL bRet = FALSE;

	m_TimeOut = uTimeOut;
	m_StartTickCount = GetTickCount();

	// Call base class
	bRet = CSocket::Connect(szServer, uPort);
	return bRet;
}

int CSocketX::Send(LPCTSTR lpszStr, UINT uTimeOut, int nFlags)
{
	m_TimeOut = uTimeOut;
	m_StartTickCount = GetTickCount();

	// Call base class function
	int nRet = CSocket::Send(lpszStr, (int)strlen(lpszStr), nFlags);

	if (GetLastError() == WSAEINTR)
			SetLastError(WSAETIMEDOUT);

	return nRet;
}


int CSocketX::Receive(CString& str, CString& EndString, UINT uTimeOut, int nFlags)
{
static char szBuf[1024];
int nRet = 1;
bool bDone = FALSE;

	str.Empty();

	while (nRet != SOCKET_ERROR && !bDone)
		{
		m_TimeOut = uTimeOut;
		m_StartTickCount = GetTickCount();
		memset(szBuf, 0, sizeof(szBuf));

		// Call base class function
		nRet = CSocket::Receive(szBuf, sizeof(szBuf), nFlags);

		if (nRet != SOCKET_ERROR)
			{
			szBuf[nRet] = 0;
			str += szBuf;
			if (!EndString.IsEmpty())
				bDone = (str.Find(EndString) != -1);
			else
				bDone = nRet < 1023;
			}

		if (nRet == SOCKET_ERROR)
			{
			if (GetLastError() == WSAEINTR)
				SetLastError(WSAETIMEDOUT);
			}
		}

	return nRet;
}

BOOL CSocketX::OnMessagePending() 
{
	//Check the tick count... If we timeout then stop the call.
	if (GetTickCount() - m_StartTickCount > m_TimeOut)
		CSocket::CancelBlockingCall();

	return CSocket::OnMessagePending();
} 


